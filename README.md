# SIAS360 Doc

[![Netlify Status](https://api.netlify.com/api/v1/badges/1a1a8b40-fb87-43ce-aa2a-82a5f4c04cef/deploy-status)](https://app.netlify.com/sites/sias360/deploys)

A template to get started with public doc for being transparent of GDPR policies.

## Features

1. Git based platform
2. i18n out of box
3. Admin panel for content management
4. Dark/Light mode