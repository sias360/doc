import theme from "@nuxt/content-theme-docs";

export default theme({
  docs: {
    primaryColor: "#E24F55",
  },
  i18n: {
    locales: () => [
      {
        code: "pt",
        iso: "pt-PT",
        name: "Português",
      },
      {
        code: "en",
        iso: "en-US",
        file: "en-US.js",
        name: "English",
      },
    ],
    defaultLocale: "en",
  },
  pwa: {
    manifest: {
      name: "SIAS360",
    },
  },
});
