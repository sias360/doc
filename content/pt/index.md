---
title: Introdução
position: 1
category: ""
---
<img src="/images/sias-360.jpg" class="light-img" width="1280" height="640" alt=""/>
<img src="/images/sias-360.jpg" class="dark-img" width="1280" height="640" alt=""/>

Modelo para [](https://github.com/MexsonFernandes/nuxt-netlify-doc)[SIAS 360 Platform](https://sias.preflet.com).

<alert type="sucesso">Sua documentação foi criada com sucesso!</alert>

## Características

<list :items="\\\['Git based', 'Admin Panel', 'i18n support', 'Article workflow', 'Team collaboration']"></list>

<p class="flex items-center"> Aproveite o modo claro e escuro: &nbsp; <app-color-switcher class="inline-flex ml-2"> </app-color-switcher></p>