---
title: Começando
description: null
position: 2
category: Básico
---

O Smart Integrated Access System é uma solução ponta a ponta de gerenciamento de controle de acesso. Temos uma solução sem contato para ajudá-lo a começar sem se preocupar com o gerenciamento de acesso.

## Começar

Para saber mais sobre a plataforma, visite [aqui](https://sias.preflet.com).

## Instalar como um aplicativo

O aplicativo é um Progressive Web App e pode ser instalado como um aplicativo nativo. Funciona offline também.
