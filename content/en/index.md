---
title: Introduction
description: ""
position: 1
category: " "
---
![](/images/sias-360.jpg)

Information hub of [SIAS 360 Platform](https://sias.preflet.com).

<alert type="success">

Learn everything about the platform!

</alert>

## Features

<list :items="\\['Body Temperature Assessment', 'Mask-wearing identification', 'Facial Recognition', 'Access Control', 'Time &Attendance', 'Real-time Alerts']"></list>

<p class="flex items-center">Enjoy light and dark mode:&nbsp;<app-color-switcher class="inline-flex ml-2"></app-color-switcher></p>