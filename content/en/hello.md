---
title: Getting started
description: Smart Integrated Access System is an end to end solution of access
  control management. We have contactless solution to help you get started
  without worrying about access management.
position: 2
category: Basic
---

Smart Integrated Access System is an end to end solution of access control management. We have contactless solution to help you get started without worrying about access management.

## Start

To learn about the platform, visit [here](https://sias.preflet.com).

## Install as an app

The application is a Progressive Web App and can be installed like a native application. It works offline as well.
