module.exports = {
  theme: {
    extend: {
      colors: {
        primary: {
          100: "#f2f2f2",
          200: "#d9d9d9",
          300: "#bfbfbf",
          400: "#a6a6a6",
          500: "#8c8c8c",
          600: "#737373",
          700: "#404040",
          800: "#262626",
          900: "#0d0d0d",
        },
      },
      fontFamily: {
        mono: ["PT Mono"],
      },
    },
  },
};
